# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Rob McCathie <rob[at]manjaro[dot]org>
# Contributor: Alexandre Arnt <arnt[at]manjaro[dot]org>
# Contributor: Roland Singer <roland[at]manjaro[dot]org>
# Contributor: Ramon Buldó <ramon[at]manaro[dot]org>
# Contributor: Tomasz Przybył <fademind[at]manaro[dot]org>

pkgname=octopi
pkgver=0.16.2
pkgrel=4
pkgdesc="A powerful Pacman frontend using Qt libs"
arch=('x86_64')
url="https://tintaescura.com/projects/octopi"
license=('GPL-2.0-or-later')
depends=(
  'alpm-octopi-utils'
  'qt-sudo'
  'qt6-5compat'
  'qtermwidget'
)
makedepends=(
  'cmake'
  'git'
  'qt6-tools'
)
optdepends=(
  'inxi: for SysInfo log'
  'lsb-release: for SysInfo log'
  'mhwd: for SysInfo log'
#  'pacaur: for AUR support'
  'pacmanlogviewer: to view pacman log files'
#  'paru: for AUR support'
#  'pikaur: for AUR support'
  'systemd: for SysInfo log'
#  'trizen: for AUR support'
  'yay: for AUR support'
)
provides=(
  'octopi-cachecleaner'
  'octopi-notifier'
  'octopi-repoeditor'
)
conflicts=(
  'octopi-notifier'
)
replaces=(
  'octopi-notifier-frameworks'
  'octopi-notifier-qt5'
)
source=("$pkgname-$pkgver.tar.gz::https://github.com/aarnt/octopi/archive/refs/tags/v$pkgver.tar.gz")
sha256sums=('7a10e68c0eba817d3c5917a392034c9d92dd975f4f2eaf9343b3ae35701e2c93')

prepare() {
  cd "$pkgname-$pkgver"

  # Don't hardcode qt-sudo path
  sed -i 's/usr\/local/usr/g' src/constants.h
}

build() {
  cmake -B build -S "$pkgname-$pkgver" \
    -DCMAKE_BUILD_TYPE='None' \
    -DCMAKE_INSTALL_PREFIX='/usr' \
    -Wno-dev
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build

  # remove duplicate license
  rm -r "$pkgdir/usr/share/licenses"
}
